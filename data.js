export const projects = [
  {
    title: "Decentralized Games",
    subtitle: "Blockchain",
    description:
      "Experimental blockchain games using smart contracts deployed on Ethereum and Binance Smart Chain networks. Current contracts are on Rinkeby testnet.",
    image: "/project-1.gif",
    link: "/Web3",
  },
  {
    title: "Google Clone",
    subtitle: "Next.js and Tailwind CSS",
    description:
      "A functional clone of Google.com using React with Next.js and Tailwind CSS for practice using inline HTML styling. Deployment with Vercel and hosting on Hostinger.",
    image: "/project-2.gif",
    link: "https://github.com/itripleg",
  },
  {
    title: "Facebook Clone",
    subtitle: "React and Firebase",
    description:
      "More practice building clones of popular websites. This clone has image uploading and user authentication and Cloud Firestore integration. Usage of Redux for state management. ",
    image: "/project-3.gif",
    link: "https://github.com/itripleg",
  },
  {
    title: "Crypto Currency",
    subtitle: "DeFi",
    description:
      "Experimental ERC20 and BEP20 smart contract based tokens. Yeild farming and self governing ecosystems. Flash loans and arbitrage bots. Reflection tokens.",
    image: "/project-4.gif",
    link: "https://github.com/itripleg",
  },
];

export const skills = [
  "Web Languages: JavaScript,  HTML,  PHP,  XML",
  "Blockchain Tech: Web3, Solidity, Moralis.io, Truffle",
  "Web design: React,  Next.js,  Tailwind CSS,  Material UI",
  "Programming: Node, C++, Python",
  "Database Management: SQL, Moralis, Firebase, MongoDB",
  "Scripting: Bash, Powershell, Docker"
];
